// Bar chart(Marius)
new Chart($("#bar-chart"), {
    type: 'bar',
    data: {
      labels: ["Volkswagen Golf", "Renault Clio", "Volkswagen Polo", "Ford Fiesta", "Nissan Qashqai"],
      datasets: [
        {
          label: "The best-selling cars in Europe 2017 (units)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [445206,298990,255370,237770,230860]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'The best-selling cars in Europe 2017 (units)'
      }
    }
});

// Kodas sukuria pie charta(Marius)
new Chart($("#pie-chart"), {
    type: 'pie',
    data: {
      labels: ["Norway", "Germany", "Canada", "United States", "Netherlands"],
      datasets: [{
        label: "Top 5 countries in 2018  Winter Olypics (medals)",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        data: [39,31,29,23,20]
      }]
    },
    options: {
      title: {
        display: true,
        text: "Top 5 countries in 2018  Winter Olympics (medals)"
      }
    }
});

// Line chart(Marius)
new Chart($("#line-chart"), {
  type: 'line',
  data: {
    labels: [2008,2009,2010,2011,2012,2013,2014,2015,2016,2017],
    datasets: [{ 
        data: [1547,1729,1991,2184,2424,2631,2880,3150,3385,3578],
        label: "World",
        borderColor: "#3e95cd",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Number of internet users worldwide from 2008 to 2017 (in millions)'
    }
  }
});

// Bar chart horizontal(Marius)
new Chart($("#bar-chart-horizontal"), {
    type: 'horizontalBar',
    data: {
      labels: ["Bill Gates", "Warren Buffett", "Jeff Bezos", "Amancio Ortega", "Mark Zuckerberg"],
      datasets: [
        {
          label: "The World's Richest Billionaires 2017 (billions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [86.0,75.6,72.8,71.3,56.0]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: "The World's Richest Billionaires 2017 (billions)"
      }
    }
});


///Ruta charts
//doughnut-chart

new Chart(document.getElementById("doughnut-chartRuta"), {
    type: 'doughnut',
    data: {
      labels: ["Boredom", "Thrill", "Temperature", "Fun", "Snow"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Reasons to take on winter hobbies'
      }
    }
});

//Line chart

new Chart(document.getElementById("line-chartRuta"), {
  type: 'line',
  data: {
    labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
    datasets: [{ 
        data: [86,114,106,106,107,111,133,221,783,2478],
        label: "Africa",
        borderColor: "#3e95cd",
        fill: false
      }, { 
        data: [282,350,411,502,635,809,947,1402,3700,5267],
        label: "Asia",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: [168,170,178,190,203,276,408,547,675,734],
        label: "Europe",
        borderColor: "#3cba9f",
        fill: false
      }, { 
        data: [40,20,10,16,24,38,74,167,508,784],
        label: "Latin America",
        borderColor: "#e8c3b9",
        fill: false
      }, { 
        data: [6,3,2,2,7,26,82,172,312,433],
        label: "North America",
        borderColor: "#c45850",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'People partaking in winter sports per annum (in millions)'
    }
  }
});


//radar chart

new Chart(document.getElementById("radar-chartRuta"), {
    type: 'radar',
    data: {
      labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
      datasets: [
        {
          label: "1950",
          fill: true,
          backgroundColor: "rgba(179,181,198,0.2)",
          borderColor: "rgba(179,181,198,1)",
          pointBorderColor: "#fff",
          pointBackgroundColor: "rgba(179,181,198,1)",
          data: [8.77,55.61,21.69,6.62,6.82]
        }, {
          label: "2050",
          fill: true,
          backgroundColor: "rgba(255,99,132,0.2)",
          borderColor: "rgba(255,99,132,1)",
          pointBorderColor: "#fff",
          pointBackgroundColor: "rgba(255,99,132,1)",
          pointBorderColor: "#fff",
          data: [25.48,54.16,7.61,8.06,4.45]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Distribution of winter sports enthusiasts per region and year'
      }
    }
});

//horizontal bar chart

new Chart(document.getElementById("bar-chart-horizontalRuta"), {
    type: 'horizontalBar',
    data: {
      labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Predicted increase of winter sports enthusiasts by 2050'
      }
    }
});

///Ruta charts end

// Bar chart(Arunas)
new Chart(document.getElementById("doughnut-chartArunas"), {
    type: 'doughnut',
    data: {
      labels: ["Boredom", "Thrill", "Temperature", "Fun", "Snow"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Reasons to take on winter hobbies'
      }
    }
});
